import React, {Component} from 'react';
import { StyleSheet, View,Text,FlatList, SafeAreaView} from 'react-native';
import Card from './card.js';
import card from './card.js';
export default class App extends Component{
  constructor(){
    super();
    this.state={
      data:[
        {
          id:'0',
          title:'Create Community'
        },
        {
          id:'1',
          title:'Offers'
        },
        {
          id:'2',
          title:'Post Product'
        },
        {
          id:'3',
          title:'Create Skill'
        },
      
      ]
    }
  }
  renderItem = ({ item }) => {
    return(
      <View>
        <Card/>
        <Text style={styles.text}>{item.title}</Text>
      </View>
    )
  }
  render(){
    return(
      <SafeAreaView>
      <FlatList
      numColumns={2}
       data={this.state.data}
       renderItem={this.renderItem}
       keyExtractor={item => item.id}
     />
     </SafeAreaView>
      
    )
  }
}
const styles = StyleSheet.create({
  text:{
    fontSize:16,
    fontWeight:'normal',
    textAlign:'left',
  }
})